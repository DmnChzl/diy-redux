import StateProvider from './StateProvider';
import StateConsumer from './containers/StateConsumer';

function App() {
  return (
    <StateProvider>
      <StateConsumer />
    </StateProvider>
  );
}

export default App;
