import { createContext, useReducer, useEffect } from 'react';
import { node } from 'prop-types';

export const ACTIONS = {
  INCREMENT_INDEX: 'CURRENT_IDX/INCREMENT_INDEX',
  DECREMENT_INDEX: 'CURRENT_IDX/DECREMENT_INDEX',
  SET_FIELD: 'FIELD_VALUES/SET_FIELD'
};

const initialState = {
  currentIdx: 0,
  fieldValues: {
    email: 'morty.smith@pm.me'
  }
};

export const store = createContext(initialState);

const { Provider } = store;

const rootReducer = (state, { type, payload }) => {
  switch (type) {
    case ACTIONS.INCREMENT_INDEX:
      return {
        ...state,
        currentIdx: state.currentIdx + 1
      };

    case ACTIONS.DECREMENT_INDEX:
      return {
        ...state,
        currentIdx: state.currentIdx - 1
      };

    case ACTIONS.SET_FIELD:
      return {
        ...state,
        fieldValues: {
          ...state.fieldValues,
          [payload.key]: payload.value
        }
      };

    default:
      throw new Error();
  }
};

function StateProvider({ children }) {
  const [state, dispatch] = useReducer(rootReducer, initialState);

  useEffect(() => {
    console.log('state', state);
  }, [state]);

  return <Provider value={{ state, dispatch }}>{children}</Provider>;
}

StateProvider.propTypes = {
  children: node
};

export default StateProvider;
