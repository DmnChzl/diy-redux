import { number, bool, node } from 'prop-types';

function Elevation({ deskSize, tabSize, isReversed, children }) {
  const isSizeDesk = deskSize ? `is-${deskSize}-desktop` : '';
  const isOffSetDesk = deskSize ? `is-offset-${(12 - deskSize) / 2}-desktop` : '';
  const isSizeTab = tabSize ? `is-${tabSize}-tablet` : '';
  const isOffSetTab = tabSize ? `is-offset-${(12 - tabSize) / 2}-tablet` : '';

  return (
    <div className={`column ${isSizeDesk} ${isOffSetDesk} ${isSizeTab} ${isOffSetTab} with-elevation`}>
      <div className={`columns ${isReversed ? 'reverse-columns' : ''}`}>{children}</div>
    </div>
  );
}

Elevation.defaultProps = {
  isReversed: false
};

Elevation.propTypes = {
  deskSize: number,
  tabSize: number,
  isReversed: bool,
  children: node
};

export default Elevation;
