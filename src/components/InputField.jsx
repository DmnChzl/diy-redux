import { string, bool, func } from 'prop-types';
import Field from './Field';

function InputField({ label, type, placeholder, defaultValue, isRequired, handleChange }) {
  return (
    <Field label={label} isRequired={isRequired}>
      <input
        className="input"
        type={type}
        placeholder={placeholder}
        defaultValue={defaultValue}
        onChange={handleChange}
      />
    </Field>
  );
}

InputField.defaultProps = {
  type: 'text',
  isRequired: false
};

InputField.propTypes = {
  label: string,
  type: string,
  placeholder: string,
  defaultValue: string,
  isRequired: bool,
  handleChange: func
};

export default InputField;
