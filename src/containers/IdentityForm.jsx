import { string } from 'prop-types';
import Form from '../components/Form';
import InputField from '../components/InputField';
import useIndex from '../hooks/useIndex';
import useField from '../hooks/useField';

function IdentityForm(props) {
  const { incrementIndex } = useIndex();
  const [lastName, setLastName] = useField('lastName');
  const [firstName, setFirstName] = useField('firstName');

  return (
    <Form {...props} handleNext={incrementIndex} isDisabled={!lastName || !firstName}>
      <InputField
        label="LastName"
        placeholder="Abramov"
        defaultValue={lastName}
        isRequired
        handleChange={e => setLastName(e.target.value)}
      />
      <InputField
        label="FirstName"
        placeholder="Dan"
        defaultValue={firstName}
        isRequired
        handleChange={e => setFirstName(e.target.value)}
      />
    </Form>
  );
}

IdentityForm.propTypes = {
  title: string,
  description: string
};

export default IdentityForm;
