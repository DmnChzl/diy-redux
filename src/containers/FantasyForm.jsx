import { string } from 'prop-types';
import Form from '../components/Form';
import InputField from '../components/InputField';
import SelectField from '../components/SelectField';
import useIndex from '../hooks/useIndex';
import useField from '../hooks/useField';

function FantasyForm(props) {
  const { incrementIndex, decrementIndex } = useIndex();
  const [nickName, setNickName] = useField('nickName');
  const [job, setJob] = useField('job');
  const [element, setElement] = useField('element');

  return (
    <Form {...props} handlePrevious={decrementIndex} handleNext={incrementIndex} isDisabled={!job || !element}>
      <InputField
        label="NickName"
        placeholder="Cliff"
        defaultValue={nickName}
        handleChange={e => setNickName(e.target.value)}
      />
      <SelectField
        label="Job"
        options={['Bowman', 'Knight', 'Paladin', 'Wizard']}
        defaultValue={job}
        isRequired
        handleChange={e => setJob(e.target.value)}
      />
      <SelectField
        label="Element"
        options={['Fire', 'Rock', 'Earth', 'Wind', 'Water', 'Ice', 'Lightning']}
        defaultValue={element}
        isRequired
        handleChange={e => setElement(e.target.value)}
      />
    </Form>
  );
}

FantasyForm.propTypes = {
  title: string,
  description: string
};

export default FantasyForm;
