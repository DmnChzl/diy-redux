const WARN = 1;

module.exports = {
  extends: ['react-app', 'react-app/jest', 'prettier', 'prettier/react'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': [
      WARN,
      {
        printWidth: 120,
        singleQuote: true,
        trailingComma: 'none',
        jsxBracketSameLine: true,
        arrowParens: 'avoid'
      }
    ]
  }
};
